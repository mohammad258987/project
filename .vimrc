"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"colorscheme gruvbox
" => Open terminal inside Vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"map <Leader>tt :vnew term://fish<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"=> Mouse Scrolling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set mouse=nicr
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" 
" The lightline.vim theme
"let g:lightline = {
"      \ 'colorscheme': 'gruvbox',
"      \ }
" Always show statusline
set laststatus=2

"Set compatibility to Vim only.


set nocompatible
"""""""""""""""""
"filetype plugin on
let g:python_highlight_all = 1
" Turn on syntax highlighting.
"syntax on
"colorscheme solarized
" Turn off modelines
set modelines=0


" Automatically wrap text that extends beyond the screen length.
"set wrap


" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F2> key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>
" Copy selected text to system clipboard
vnoremap <leader>y :!xclip -selection clipboard<CR>
" Paste from system clipboard
nnoremap <leader>p :r !xclip -o -selection clipboard<CR>
"#########################################################################################

let g:airline_theme = 'catppuccin_mocha'
let g:lightline = {'colorscheme': 'catppuccin_mocha'}
call plug#begin('~/.vim/plugged')

"Fugitive Vim Github Wrapper
Plug 'tpope/vim-fugitive'
Plug 'vimwiki/vimwiki'
Plug 'godlygeek/tabular'
"Plug 'tpope/vim-markdown'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/seoul256.vim'
Plug 'ap/vim-css-color'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'morhetz/gruvbox'
Plug 'kovetskiy/sxhkd-vim'
Plug 'suan/vim-instant-markdown', {'rtp': 'after'} " Markdown Preview
Plug 'VundleVim/Vundle.vim'
Plug 'lervag/vimtex'
Plug 'vim-python/python-syntax'                    " Python highlighting
Plug 'junegunn/vim-emoji'                          " Vim needs emojis!
" Add Dracula colorscheme
Plug 'dracula/vim', { 'name': 'dracula' }
Plug 'itchyny/lightline.vim'                       " Lightline statusbar
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'     " Highlighting Nerdtree
Plug 'ryanoasis/vim-devicons'                      " Icons for Nerdtree
Plug 'catppuccin/vim', { 'as': 'catppuccin' }
call plug#end()

colorscheme industry
colorscheme gruvbox
"colorscheme dracula
"colorscheme catppuccin_mocha






"########################################################################################



" Show line numbers
"set number


" Encoding
set encoding=utf-8


" Call the .vimrc.plug file
if filereadable(expand("~/.vimrc.plug"))
    source ~/.vimrc.plug
endif


" make calcurse notes markdown:
"autocmd BufRead,BufNewFile *.tex set filetype=tex
"autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown

" ##################DTOS CONFIG########################

"set t_Co=256                    " Set if term supports 256 colors.

set number       " Display line numbers

set clipboard=unnamedplus       " Copy/paste between vim and other programs.

" The lightline.vim theme

"let g:lightline = {

"     \ 'colorscheme': 'darcula',

"      \ }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" => Colors and Theming

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

highlight LineNr           ctermfg=8    ctermbg=none    cterm=none

"highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none

"highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none

"highlight Statement        ctermfg=2    ctermbg=none    cterm=none




"set guioptions-=m  "remove menu bar

"set guioptions-=T  "remove toolbar

"set guioptions-=r  "remove right-hand scroll bar


"set guioptions-=L  "remove left-hand scroll bar




""chatgpt"""
" Use spaces instead of tabs
set expandtab

" Set the number of spaces per tab (e.g., 4 spaces)
set tabstop=4
set shiftwidth=4
set softtabstop=4

" Map <F5> to compile and run C++ programs
nnoremap <F5> :w<CR>:!g++ % -o %< && ./%< <CR>
"Run the Python code
nnoremap <F6> :w<CR>:!python %<CR>
"Run the java code
nnoremap <F9> :w<CR>:!javac % && java %:r<CR>
" Map F5 to save and run the current Bash script
nnoremap <F7> :w<CR>:!bash %<CR>

nnoremap <F8> :w<CR>:!lua %<CR>
" Map a key combination to run Octave script
nnoremap <F10> :w<CR>:!octave -q %<CR>
" Map a key to compile and view LaTeX files
autocmd FileType tex nnoremap <leader>lc :VimtexCompile<CR>
autocmd FileType tex nnoremap <leader>lv :VimtexView<CR>







""""""""
syntax enable
filetype plugin indent on
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" => VimWiki

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimwiki_list = [{'path': '~/Documents/vimwiki/',
                     \ 'syntax': 'markdown', 'ext': '.md'}]

au FileType vimwiki setlocal shiftwidth=6 tabstop=6 noexpandtab

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"let g:vimwiki_list = [{'path': '~/vimwiki/',
"                      \ 'ext': '.md'}]






""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Customize the colors for Vimwiki elements
" Example Changing the header color to blue
"highlight VimwikiHeader guifg=blue

"This option will treat all markdown files in your system as part of vimwiki (check set filetype?). Add


"let g:vimwiki_global_ext = 0
"vim.cpp - additional vim c++ syntax highlighting
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_posix_standard = 1
let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1
let g:cpp_no_function_highlight = 1
let c_no_curly_error=1

filetype plugin on
"Uncomment to override defaults:
let g:instant_markdown_slow = 1
let g:instant_markdown_autostart = 0
"let g:instant_markdown_open_to_the_world = 1
"let g:instant_markdown_allow_unsafe_content = 1
"let g:instant_markdown_allow_external_content = 0
"let g:instant_markdown_mathjax = 1
"let g:instant_markdown_mermaid = 1
"let g:instant_markdown_logfile = '/tmp/instant_markdown.log'
"let g:instant_markdown_autoscroll = 0
"let g:instant_markdown_port = 8888
"let g:instant_markdown_python = 1
let g:instant_markdown_theme = 'dark'

" Enable auto-pairs
let g:AutoPairsStartInsertMode = 1



